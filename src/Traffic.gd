extends Node2D

const CAR = preload('res://Car.tscn')
const COIN = preload('res://Coin.tscn')

onready var Road = get_node('/root/Game/Road')
onready var Player = get_node('/root/Game/Player')
onready var Timer = get_node('/root/Game/TrafficTimer')
onready var CoinTimer = get_node('/root/Game/CoinTimer')

var lanes = []
onready var lane_num = Road.width*2

func _ready():
	randomize()
	for i in range(lane_num):
		lanes.append(null)
	Timer.connect('timeout', self, 'spawn')
	CoinTimer.connect('timeout', self, 'spawn_coin')
	Timer.start()
	CoinTimer.start()

func roll_vel(v_p):
	# var v = 0
	# for i in range(4):
		# v += (randi() % 200) + 1
	# v = v - 400 + v_p
	# return v
	return (randi() % 400) - 200 + v_p

func lane_to_pos(lane):
	return get_viewport().size.y - (2*Road.TILE_HALF*(lane - Player.lane) + Player.position.y)

func spawn():
	var free_lanes = []
	for i in range(len(lanes)):
		if lanes[i]: continue
		free_lanes.append(i)
	if free_lanes.empty(): return

	var car = CAR.instance()
	var v_p = Player.velocity.x
	var v = roll_vel(v_p)
	if v < v_p:
		car.position.x = get_viewport().size.x + Road.TILE_HALF
	else:
		car.position.x = -Road.TILE_HALF
	var lane = free_lanes[randi() % len(free_lanes)]
	car.position.y = lane_to_pos(lane)
	car.velocity.x = v
	add_child(car)
	car.connect('out_of_bounds', self, 'destroy_car')
	lanes[lane] = car

func spawn_coin():
	var lane = randi() % lane_num
	var coin = COIN.instance()
	coin.position.x = get_viewport().size.x + Road.TILE_HALF
	coin.position.y = lane_to_pos(lane)
	add_child(coin)

func destroy_car(car):
	for i in range(len(lanes)):
		if lanes[i] != car: continue
		lanes[i] = null
		car.queue_free()

