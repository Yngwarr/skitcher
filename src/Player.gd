extends KinematicBody2D

signal velocity_changed
signal changing_lane
signal dead

enum Direction { UP, DOWN }

var SWAP_SPEED = 400

var velocity = Vector2()
var lane = 0
var can_grip = []
var gripping = null

onready var DecelerateTimer = $DecelerateTimer

func _ready():
	add_to_group('player')
	$Area.add_to_group('coin_taker')
	accelerate(300)
	DecelerateTimer.connect('timeout', self, 'decelerate')
	DecelerateTimer.start()

func accelerate(x_vel):
	velocity.x += x_vel
	emit_signal('velocity_changed', velocity)

func change_lane(dir):
	if lane == 0 and dir == Direction.DOWN:
		# TODO play animation
		return
	if lane == 5 and dir == Direction.UP:
		# TODO play animation
		return
	ungrip()
	velocity.y = -SWAP_SPEED if dir == Direction.UP else SWAP_SPEED
	emit_signal('velocity_changed', velocity)
	lane += -1 if dir == Direction.DOWN else 1
	emit_signal('changing_lane', lane)

func stop_y():
	velocity.y = 0
	emit_signal('velocity_changed', velocity)

func add_grip_info(car):
	can_grip.append(car)

func rm_grip_info(car):
	can_grip.erase(car)

func grip():
	if gripping:
		ungrip()
		return
	if can_grip.empty(): return
	gripping = can_grip.back()
	accelerate(gripping.velocity.x - velocity.x + 100)

func ungrip():
	# if gripping == null: return
	gripping = null

func decelerate():
	if gripping: return
	accelerate(-15)
	if velocity.x <= 0:
		die()

func die():
	emit_signal('dead')
