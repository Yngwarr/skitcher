extends KinematicBody2D

signal taken

onready var Game = get_node('/root/Game')
onready var Player = get_node('/root/Game/Player')

func _ready():
	connect('taken', Game, 'coin_taken')
	$Area.connect('area_entered', self, 'take')
	# Player.connect('velocity_changed', self, 'compensate_movement')
	$AnimationPlayer.play('idle')

func _physics_process(delta):
	move_and_slide(-Player.velocity)
	if position.x <= -50:
		queue_free()

func take(body):
	if not body.is_in_group('coin_taker'): return
	emit_signal('taken')
	queue_free()
