extends KinematicBody2D

enum Traps {PIT, CONE, BLOCK}

const TILES = {
	'lane': preload('res://tiles/Lane.tscn'),
	'road': preload('res://tiles/Road.tscn'),
	'road_up': preload('res://tiles/RoadUp.tscn'),
	'solid': preload('res://tiles/Solid.tscn'),
	'grass': preload('res://tiles/Grass.tscn')
}
const SIGHT_RANGE = 9

export (int) var TILE_HALF = 125

# ==== GENERATION ====
var road_len = 200

var one_side_edge = 100
var one_side_prob = 0.2
var one_side_len = 50

export (int) var width_len = 50
var width_prob = 0.3
var width_min = 1
var width_max = 4

# ====================

var width = 3
var one_side = false
var current_step = 0
var velocity = Vector2(0, 0)
var road_y = -8*TILE_HALF
var stop_at_y = 0

var events = []

onready var Player = get_node('/root/Game/Player')
onready var Traffic = get_node('/root/Game/Traffic')
onready var Asphalt = $Asphalt.get_children()
onready var Tiles = $Tiles

func roll(p):
	if randf() <= p: return true
	else: return false

func _ready():
	randomize()
	Player.connect('velocity_changed', self, 'change_velocity')
	Player.connect('changing_lane', self, 'stop_at_lane')
	events += mk_events(road_len)
	print(events)
	while current_step < SIGHT_RANGE:
		spawn_tiles(current_step, current_step, 0)
		current_step += 1

func _physics_process(delta):
	# stopping lane change when already done
	if velocity.y > 0 and stop_at_y < road_y:
		velocity.y = 0
		Player.stop_y()
	if velocity.y < 0 and stop_at_y > road_y:
		velocity.y = 0
		Player.stop_y()

	road_y += velocity.y * delta
	for a in Asphalt:
		a.move_and_slide(velocity)
		if a.position.x < -a.teleport_edge.x:
			a.position.x += 6 * a.teleport_edge.x
		if a.position.x > 5 * a.teleport_edge.x:
			a.position.x -= 6 * a.teleport_edge.x
		if a.position.y < -a.teleport_edge.y:
			a.position.y += 4 * a.teleport_edge.y
		if a.position.y > 3 * a.teleport_edge.y:
			a.position.y -= 4 * a.teleport_edge.y
	for i in range(Tiles.get_child_count()):
		var t = Tiles.get_child(i)
		t.position += velocity * delta
		if t.position.x < -TILE_HALF:
			t.queue_free()
			if i == 0:
				spawn_tiles(current_step, SIGHT_RANGE, t.position.x + TILE_HALF)
				current_step += 1

# connected to a signal
func change_velocity(vel):
	velocity = -vel

# connected to a signal
func stop_at_lane(lane):
	stop_at_y = 2*TILE_HALF*(lane - 4)

func mk_events(rlen):
	var mute_one_side = one_side_edge
	var mute_width = width_len
	var res = []
	res.append({step = 0, width = width, one_side = one_side})

	var event = {}
	var loc_width = width
	var loc_one_side = one_side
	for i in range(rlen):
		event = {}

		if mute_one_side > 0:
			mute_one_side -= 1
		elif roll(one_side_prob):
			loc_one_side = !loc_one_side
			mute_one_side = one_side_len
			event['one_side'] = loc_one_side
		else:
			mute_one_side = 5 + randi() % 10

		if mute_width > 0:
			mute_width -= 1
		elif roll(width_prob):
			if loc_width == width_min:
				loc_width += 1
			elif loc_width == 2 and loc_one_side:
				loc_width += 1
			elif loc_width == width_max:
				loc_width -= 1
			else:
				loc_width += 1 if roll(0.5) else -1
			mute_width = width_len
			event['width'] = loc_width
		else:
			mute_width = 3 + randi() % 10

		# TODO traps

		if len(event.keys()) == 0: continue
		event['step'] = i
		res.append(event)
	return res

func spawn_tiles(step, tile_x, offset):
	var e = events.front()
	if e and step == e.step:
		# TODO add traps
		events.pop_front()
	for i in range(-1, width*2 + 2):
		var t = null
		if i == 0:
			t = TILES['road_up'].instance()
		elif i == width:
			t = TILES['solid'].instance()
		elif i == width*2:
			t = TILES['road'].instance()
		elif i == -1 or i == width*2 + 1:
			t = TILES['grass'].instance()
		else:
			t = TILES['lane'].instance()
		t.position.x = 2*TILE_HALF*tile_x - TILE_HALF + offset
		t.position.y = road_y + 2*TILE_HALF*i + TILE_HALF
		Tiles.add_child(t)
