extends KinematicBody2D

var teleport_edge = Vector2()

func _ready():
	var size = $Sprite.get_rect().size
	teleport_edge.x = size.x / 2
	teleport_edge.y = size.y / 2
