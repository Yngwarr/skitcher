extends KinematicBody2D

signal out_of_bounds

onready var Player = get_node('/root/Game/Player')
onready var Road = get_node('/root/Game/Road')
onready var Grip = $Grip

var velocity = Vector2(600, 0)

func _ready():
	Grip.connect('body_entered', self, 'grip_entered')
	Grip.connect('body_exited', self, 'grip_exited')
	Player.connect('velocity_changed', self, 'compensate_movement')
	$Bumper.connect('body_entered', self, 'bump')
	$AnimationPlayer.play('idle')

func _physics_process(delta):
	var real_v = velocity
	real_v.x -= Player.velocity.x
	move_and_slide(real_v)
	for i in get_slide_count():
		var col = get_slide_collision(i)
		if col.collider.is_in_group('player') and Player.gripping == self:
			Player.velocity.x = velocity.x

	if position.x <= -2*Road.TILE_HALF or position.x >= get_viewport().size.x + 2*Road.TILE_HALF:
		emit_signal('out_of_bounds', self)

func grip_entered(body):
	if body.is_in_group('player'):
		Player.add_grip_info(self)

func grip_exited(body):
	if body.is_in_group('player'):
		Player.rm_grip_info(self)

func compensate_movement(vel):
	velocity.y = -vel.y

func bump(body):
	if not body.is_in_group('player'): return
	Player.die()
