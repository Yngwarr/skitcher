extends Node2D

onready var Player = $Player
onready var Speed = $HUD/Speed
onready var Score = $HUD/Score
onready var GameOver = $HUD/GameOver

var score = 0

func _ready():
	Player.connect('dead', self, 'game_over')

func _process(delta):
	Speed.text = "V = %d km/h" % (Player.velocity.x / 20)

func _input(event):
	if event is InputEventKey and event.pressed and not event.echo:
		match event.scancode:
			# TODO change from scancode to actions
			KEY_UP: Player.change_lane(Player.Direction.UP)
			KEY_DOWN: Player.change_lane(Player.Direction.DOWN)
			KEY_RIGHT: Player.grip()

func coin_taken():
	score += 1
	Score.text = "Score: %d" % score

func game_over():
	GameOver.visible = true;
